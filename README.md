# README #

* This is RESTful version of game of life

### How do I get set up? ###

* Run it as a usual SpringBoot application 

### How to create world ###

* To create a world send **POST** request to the link: http://localhost:8080/world
* Body should have a desired dimension of the world:

```
#!json
{
  "width": 10,
  "height": 10,
  "cells": [
    {
      "x": 1,
      "y": 2
    },
    {
      "x": 2,
      "y": 2
    },
    {
      "x": 3,
      "y": 2
    }
  ]  
}

```
* Response code **201** will be received with the world id upon its successful creation: 

```
#!json

{
  "message": "5f2416bc-121c-44a8-a42f-2f7f2bca773c"
}
```

* World will be created along with the living creatures, defined in **cells** parameter.


### How to check world generation ###
* To do it you need to send **GET** request with world id and generation number to the url like this: 
http://localhost:8080/world/5f2416bc-121c-44a8-a42f-2f7f2bca773c/generation/1
* As a result you'll receive a response with code **200**, containing all alive cells per specified generation, like this:

```
#!json
{
  "cells": [
    {
      "x": 2,
      "y": 1
    },
    {
      "x": 2,
      "y": 2
    },
    {
      "x": 2,
      "y": 3
    }
  ]
}
```