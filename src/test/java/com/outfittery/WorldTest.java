package com.outfittery;

import com.outfittery.model.Cell;
import com.outfittery.model.World;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class WorldTest {

    private World world;

    @Before
    public void setUp() throws Exception {
        world = new World(10, 10);
    }

    @Test
    public void testWorldCreated() {
        assertNotNull(world.getId());
    }

    @Test
    public void testCellWasAdded() {
        Cell cell = new Cell(1,1);
        world.addCell(cell);
        List<Cell> cells = world.getAliveCells();
        assertTrue(!cells.isEmpty());
        Cell actualCell = cells.get(0);
        assertTrue(actualCell.getX() == cell.getX() && actualCell.getY() == cell.getY());
    }

    @Test
    public void testCellWasDeleted() {
        Cell cell = new Cell(1,1);
        world.addCell(cell);
        world.makeEmpty(cell);
        List<Cell> cells = world.getAliveCells();
        assertTrue(cells.isEmpty());
    }

    @Test
    public void testCellWillBeBorn() {
        assertTrue(world.willBeBorn(3));
    }

    @Test
    public void testCellWillNotBeBorn() {
        assertFalse(world.willBeBorn(2));
    }

    @Test
    public void testCellWillSurvive() {
        assertTrue(world.willSurvive(2));
        assertTrue(world.willSurvive(3));
    }

    @Test
    public void testCellWillDie() {
        assertFalse(world.willSurvive(4));
    }

    @Test
    public void testGenerationIsAsExpected() {
        world.addCell(new Cell(1,2));
        world.addCell(new Cell(2,2));
        world.addCell(new Cell(3,2));

        List<Cell> actualCells = world.getGeneration(1);
        List<Cell> expectedCells = Arrays.asList(new Cell(2,1), new Cell(2,2), new Cell(2,3));

        assertEquals(actualCells, expectedCells);
    }

    @Test
    public void testAllDied() {
        world.addCell(new Cell(1,2));
        world.addCell(new Cell(2,2));

        List<Cell> actualCells = world.getGeneration(1);

        assertTrue(actualCells.isEmpty());
    }
}
