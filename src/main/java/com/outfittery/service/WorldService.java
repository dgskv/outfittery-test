package com.outfittery.service;

import com.outfittery.exception.EntityNotFoundException;
import com.outfittery.model.Cell;
import com.outfittery.model.World;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class WorldService {

    private Map<String, World> worldCache = new ConcurrentHashMap<>();

    public String createWorld(int width, int height) {
        World world = new World(width, height);
        worldCache.putIfAbsent(world.getId(), world);

        return world.getId();
    }

    public void addCells(String worldId, List<Cell> cells) {
        World world = getWorldById(worldId);
        cells.stream().forEach(world::addCell);
    }

    public List<Cell> getGenerationAliveCells(String worldId, Integer generationId) {
        World world = getWorldById(worldId);
        return world.getGeneration(generationId);
    }

    private World getWorldById(String worldId) {
        if (!worldCache.containsKey(worldId)) {
            throw new EntityNotFoundException("World with id " + worldId + " doesn't exist");
        }
        return worldCache.get(worldId);
    }
}
