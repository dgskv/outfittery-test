package com.outfittery.controller;

import com.outfittery.controller.dto.CellDto;
import com.outfittery.controller.dto.CellsDto;
import com.outfittery.controller.dto.DtoConvertor;
import com.outfittery.controller.dto.WorldDto;
import com.outfittery.model.Cell;
import com.outfittery.service.WorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/world")
public class GameOfLifeController {

    @Autowired
    private WorldService worldService;
    @Autowired
    private DtoConvertor dtoConvertor;

    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity createWorld(@RequestBody WorldDto worldDto) {
        String worldId = worldService.createWorld(worldDto.getWidth(), worldDto.getHeight());

        return ResponseEntity.status(HttpStatus.CREATED).body(worldId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity initLife(@PathVariable("id") String worldId, @RequestBody CellsDto cellsDto) {
        List<Cell> cells = cellsDto.getCells().stream()
                .map(dtoConvertor::dtoToCell)
                .collect(Collectors.toList());
        worldService.addCells(worldId, cells);

        return ResponseEntity.status(HttpStatus.CREATED).body("cells added");
    }

    @RequestMapping(value = "/{id}/generation/{gid}", method = RequestMethod.GET)
    public ResponseEntity getWorldGeneration(@PathVariable("id") String worldId, @PathVariable("gid") Integer generationId) {
        List<Cell> cells = worldService.getGenerationAliveCells(worldId, generationId);
        List<CellDto> cellDtos = cells.stream()
                .map(dtoConvertor::cellToDto)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new CellsDto(cellDtos));
    }
}
