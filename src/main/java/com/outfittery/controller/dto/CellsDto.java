package com.outfittery.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CellsDto {
    private List<CellDto> cells;

    public CellsDto() {
    }

    public CellsDto(List<CellDto> cells) {
        this.cells = cells;
    }

    public List<CellDto> getCells() {
        return cells;
    }

    public void setCells(List<CellDto> cells) {
        this.cells = cells;
    }
}
