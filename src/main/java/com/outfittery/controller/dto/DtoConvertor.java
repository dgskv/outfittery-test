package com.outfittery.controller.dto;

import com.outfittery.controller.dto.CellDto;
import com.outfittery.model.Cell;
import org.springframework.stereotype.Component;

@Component
public class DtoConvertor {
    public Cell dtoToCell(CellDto cellDto) {
        return new Cell(cellDto.getX(), cellDto.getY());
    }

    public CellDto cellToDto(Cell cell) {
        return new CellDto(cell.getX(), cell.getY());
    }
}
