package com.outfittery.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class World {
    private static final byte ALIVE = 1;
    private static final byte EMPTY = 0;
    private String id;
    private int width;
    private int height;
    private byte[][] board;

    public World(int width, int height) {
        this.id = UUID.randomUUID().toString();
        this.width = width;
        this.height = height;
        this.board = new byte[width][height];
    }

    public String getId() {
        return id;
    }

    public void addCell(Cell cell) {
        setCellState(cell, ALIVE);
    }

    public List<Cell> getGeneration(int generation) {
        List<Cell> dieList = new ArrayList<Cell>();
        List<Cell> bornList = new ArrayList<Cell>();

        for (int i = 0; i < generation; i++) {
            for (int x = 1; x < width - 1; x++) {
                for (int y = 1; y < height - 1; y++) {
                    int neighboursCount = getNeighboursCount(x, y);
                    if (isAlive(x, y)) {
                        if (!willSurvive(neighboursCount)) {
                            dieList.add(new Cell(x, y));
                        }
                    } else {
                        if (willBeBorn(neighboursCount)) {
                            bornList.add(new Cell(x, y));
                        }
                    }
                }
            }
            bornList.stream().forEach(this::makeAlive);
            dieList.stream().forEach(this::makeEmpty);
            bornList.clear();
            dieList.clear();

            printWorld();
            System.out.println();
        }

        return getAliveCells();
    }

    public List<Cell> getAliveCells() {
        List<Cell> cells = new ArrayList<>();
        for (int i = 0; i < width - 1; i++) {
            for (int j = 0; j < height - 1; j++) {
                if (isAlive(i, j)) {
                    cells.add(new Cell(i, j));
                }
            }
        }

        return cells;
    }

    public void makeAlive(Cell cell) {
        setCellState(cell, ALIVE);
    }

    public void makeEmpty(Cell cell) {
        setCellState(cell, EMPTY);
    }

    private void setCellState(Cell cell, byte state) {
        board[cell.getX()][cell.getY()] = state;
    }

    public boolean willBeBorn(int neighboursCount) {
        return neighboursCount == 3;
    }

    public boolean willSurvive(int neighboursCount) {
        return neighboursCount == 2 || neighboursCount == 3;
    }

    public boolean isAlive(int x, int y) {
        return board[x][y] == ALIVE;
    }

    private int getNeighboursCount(int x, int y) {
        int count = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (!isSameCell(x, y, i, j) && isAlive(i, j)) {
                    count++;
                }
            }
        }

        return count;
    }

    private boolean isSameCell(int x, int y, int i, int j) {
        return x == i && y == j;
    }

    public void printWorld() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }
}
